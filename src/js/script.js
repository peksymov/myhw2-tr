console.log("hello world");

const bars = document.querySelector('.bars');
bars.addEventListener('click', function () {
    const header = document.querySelector('.header');
    header.classList.toggle('visible');
    const showBars = document.querySelector('.fa-bars');
    showBars.classList.toggle('hide');
    const showX = document.querySelector('.fa-times');
    showX.classList.toggle('hide');
    showX.classList.toggle('visible');
})
// const showBars = document.querySelector('.fa-bars');
// showBars.addEventListener('click', function () {
//     showBars.classList.toggle('hide');
//     const showX = document.querySelector('.fa-times');
//     showX.classList.toggle('visible');
// })