let project_folder = "dist";
let source_folder = "src";

let path = {
    build: {
        html: project_folder + "/",
        css: project_folder + "/css/",
        js: project_folder + "/js/",
        img: project_folder + "/img/",
        fonts: project_folder + "/fonts/"
    },
    src: {
        html: [source_folder + "/*.html", "!"+source_folder + "/_*.html"],
        css: source_folder + "/scss/style.scss",
        js: source_folder + "/js/script.js",
        img: source_folder + "/img/**/*.{png,jpg,svg,ico}",
        fonts: source_folder + "/fonts/*.ttf"
    },
    watch: {
        html: source_folder + "/**/*.html",
        css: source_folder + "/scss/**/*.scss",
        js: source_folder + "/js/**/*.js",
        img: source_folder + "/img/**/*.{png,jpg,svg,ico}",
    },
    clean: "./" + project_folder + "/"
}

let {src, dest} = require("gulp"),   // можно записать как gulp.src или там где нужно gulp,src
    gulp = require("gulp"),
    browsersync = require("browser-sync").create(),
    fileinclude = require("gulp-file-include"),
    del = require("del"),
    scss = require("gulp-sass"),
    autoprefixer = require("gulp-autoprefixer"),
    concat = require('gulp-concat'),
    clean_css = require('gulp-clean-css'),
    rename = require("gulp-rename"),
    minify = require('gulp-minify'),
    htmlmin = require('gulp-htmlmin'),
    imagemin = require("gulp-imagemin");


function browserSync() {
    browsersync.init
    ({
        server:{
            baseDir:"./" + project_folder + "/"
        },
        port: 3000,
        notify: false
    })
}
function html () {
    return src(path.src.html)
        // .pipe(fileinclude())
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(dest(path.build.html))
        .pipe(browsersync.stream())
}
function css () {
    return src(path.src.css)
        .pipe(
            scss({
                outputStyle: "expanded"
            })
        )
        .pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
        .pipe(dest(path.build.css))
        .pipe(clean_css())
        .pipe(rename( {extname:"s.min.css"}))
        .pipe(dest(path.build.css))
        .pipe(browsersync.stream())
}

function libCss() {
    return src([
        'node_modules/@fortawesome/fontawesome-free/css/all.css',
        'node_modules/normalize.css/normalize.css'
    ])
        .pipe(concat('libs.css'))
        .pipe(clean_css())
        .pipe(gulp.dest('dist/css'));
}
function libJs() {
    return src('node_modules/@fortawesome/fontawesome-free/js/all.js')
        // .pipe(concat('all.js'))
        .pipe(minify())
        .pipe(gulp.dest('dist/js'));
}

function js () {
    return src(path.src.js)
        .pipe(minify())
        .pipe(dest(path.build.js))
        .pipe(browsersync.stream())
}
function images () {
    return src(path.src.img)
        // .pipe(fileinclude())
        .pipe(imagemin({
            progressive: true,
            // svgoPlugins: [{removeViewBox: false}],
            interlaced: true,
            optimizationLevel: 3
        })
        )
        .pipe(dest(path.build.img))
        .pipe(browsersync.stream())
}
function watchFiles () {
    gulp.watch([path.watch.html], html);
    gulp.watch([path.watch.css], css);
    gulp.watch([path.watch.js], js);
    gulp.watch([path.watch.img], images);
}

function clean () {
    return del(path.clean)
}

let build = gulp.series(clean, gulp.parallel(js, libCss, libJs, css, html, images));
let dev = gulp.parallel(build, watchFiles, browserSync);


exports.libJs = libJs;
exports.libCss = libCss;
exports.images = images;
exports.js = js;
exports.css = css;
exports.html = html;
exports.build = build;
exports.dev = dev;
exports.default = dev;
